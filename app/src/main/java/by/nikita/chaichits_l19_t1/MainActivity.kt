package by.nikita.chaichits_l19_t1

import android.Manifest
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import android.content.Intent
import android.content.pm.PackageManager
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.os.Environment
import android.os.Environment.getExternalStorageDirectory
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import by.nikita.chaichits_l19_t1.service.TestingIntentService
import by.nikita.chaichits_l19_t1.service.TestingService
import java.io.File
import java.io.FileOutputStream
import java.io.InputStream
import java.io.OutputStreamWriter


class MainActivity : AppCompatActivity() {

    val PERMISSIONS_REQUEST_CODE = 100

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val file = File(
            Environment.getExternalStorageDirectory().path +
                    "/Download/logs.txt"
        )
        file.createNewFile()
        file.writeText("LOGS: \n")

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            == PackageManager.PERMISSION_DENIED
        )
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.WRITE_EXTERNAL_STORAGE),
                PERMISSIONS_REQUEST_CODE
            )

        val btnArray = arrayOf(
            btnAdd, btnDelete, btnUpdate, btnCreate, btnPut, btnRead, btnLog,
            btnOpen, btnSave, btnEdit, btnUpload, btnLoad
        )
        btnArray.forEach { btn ->
            btn.setOnClickListener {
                val intent = Intent(this, TestingIntentService::class.java)
                intent.putExtra("BUTTONNAME", btn.text.toString())
                startService(intent)
            }
        }

        btnReadLogs.setOnClickListener {
            val inputStream: InputStream = file.inputStream()
            val inputString = inputStream.bufferedReader().use { it.readText() }
            tvLog.text = inputString
        }
    }
}
