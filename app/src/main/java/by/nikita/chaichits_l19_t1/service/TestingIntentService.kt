package by.nikita.chaichits_l19_t1.service

import android.app.IntentService
import android.content.Intent
import android.os.Environment
import android.widget.Toast
import java.io.File


class TestingIntentService : IntentService {


    constructor() : super("Hello intent service")

    override fun onHandleIntent(intent: Intent) {
        val btnName: String = intent.getStringExtra("BUTTONNAME")

        val file = File(Environment.getExternalStorageDirectory().path + "/Download/logs.txt")
        file.appendText("button " + btnName.toUpperCase() + " pressed \n")
    }
}